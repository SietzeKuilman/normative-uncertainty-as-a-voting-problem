import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random
from math import pi

#file for plotting nice things

#here are the categories:

ambiguity_categories = ['UserID','Ambiguity_SentientUtilitarianism','Ambiguity_ClassicalUtilitarianism','Ambiguity_HedonisticUtilitarianism','Ambiguity_AgentCenteredDeontology','Ambiguity_PatientCenteredDeontology','Ambiguity_ContractarianDeontology']

profile_categories = ['UserID','HowMuch_SentientUtilitarianism','HowMuch_ClassicalUtilitarianism','HowMuch_HedonisticUtilitarianism','HowMuch_AgentCenteredDeontology','HowMuch_PatientCenteredDeontology','HowMuch_ContractarianDeontology']

votes = ['UserID','Majority_wins','Borda_wins','MFT_score']

credences = ['UserID','Sentient_U_credence','Classical_U_credence','Hedonistic_U_credence','AgentCentered_D_credence','PatientCentered_D_credence','Contractarian_D_credence']


        ###############
        ## AMBIGUITY ##
        ###############

def radar_chart_ambiguity(dataframe, rank_or_country='none', compare_to_average=False):
    #radar chart for ambiguity
    profile_to_plot = dataframe.reset_index()

    if rank_or_country == 'none':
        #this is for plotting averages
        global_average = profile_to_plot.mean().to_frame().transpose()

        #here is the dataframe for ambiguity average
        global_ambiguity = global_average[ambiguity_categories]
        toplot = global_ambiguity
        rank = 0

        #useful dataframe for std
        stdbool = True
        datastd = dataframe.reset_index()

    else:
        try:
            #this is for plotting one user's profile
            #choose which one you want to see - rank between 0 and 51210
            rank = int(rank_or_country)
            #here is the dataframe for ambiguity average
            individual_ambiguity = profile_to_plot[ambiguity_categories]
            toplot = individual_ambiguity

            #useful dataframe for std
            stdbool = False

        except ValueError:
            #this is for plotting one country's profile
            country_info = profile_to_plot.groupby('UserCountry').get_group(rank_or_country).reset_index(drop=True)
            toplot = country_info.mean().to_frame().transpose()[ambiguity_categories]
            rank = 0

            #useful dataframe for std
            datastd = profile_to_plot.groupby('UserCountry').get_group(rank_or_country).reset_index(drop=True)
            stdbool = True

    #rename variables for more clarity on graph

    toplot = toplot.rename(columns={'Ambiguity_SentientUtilitarianism': 'Sentient Utilitarianism','Ambiguity_ClassicalUtilitarianism': 'Classical Utilitarianism','Ambiguity_HedonisticUtilitarianism': '                 Hedonistic Utilitarianism','Ambiguity_AgentCenteredDeontology': 'Agent-Centered Deontology','Ambiguity_PatientCenteredDeontology': 'Patient-Centered Deontology','Ambiguity_ContractarianDeontology': 'Contractarian Deontology'})

    # number of variable
    categories=list(toplot)[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot.loc[rank].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label="User")


    if compare_to_average:
        #we can compare their profile to the average if needed
        global_average = profile_to_plot.mean().to_frame()
        global_average = global_average.transpose()

        #here is the dataframe for ambiguity average
        global_ambiguity = global_average[ambiguity_categories]

        values=global_ambiguity.loc[0].drop('UserID').values.flatten().tolist()
        values += values[:1]
        ax.plot(angles, values, linewidth=1, linestyle='solid', label="Average")

        #add legend if several users or comparison with average
        plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    elif stdbool :
        #this is for plotting std
        datastd = datastd.std().to_frame().transpose()
        datastd['UserID'] = 0
        datastd = datastd[ambiguity_categories]
        datastd = datastd.rename(columns={'Ambiguity_SentientUtilitarianism': 'Sentient Utilitarianism','Ambiguity_ClassicalUtilitarianism': 'Classical Utilitarianism','Ambiguity_HedonisticUtilitarianism': '                 Hedonistic Utilitarianism','Ambiguity_AgentCenteredDeontology': 'Agent-Centered Deontology','Ambiguity_PatientCenteredDeontology': 'Patient-Centered Deontology','Ambiguity_ContractarianDeontology': 'Contractarian Deontology'})
        dataup = toplot + datastd
        datainf = toplot - datastd
        datainf[datainf < 0] = 0

        valuesup = dataup.loc[0].drop('UserID').values.flatten().tolist()
        valuesup += valuesup[:1]

        valueinf = datainf.loc[0].drop('UserID').values.flatten().tolist()
        valueinf += valueinf[:1]
        plt.fill_between(angles, valueinf, valuesup, alpha=0.5)

    if rank_or_country == 'none':
        #title
        plt.title("Average ambiguity",fontsize=12, y=1.0)
    else:
        try:
            individual = int(rank_or_country)
            #title
            plt.title("Ambiguity - User " + profile_to_plot['UserID'].loc[individual] + " from " + profile_to_plot['UserCountry'].loc[individual],fontsize=12, y=1.0)
        except ValueError:
            #title
            plt.title("Ambiguity - " + rank_or_country,fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()


def radar_chart_ambiguity_2countries(dataframe, country):
    #radar chart for ambiguity between 2 countries
    profile_to_plot = dataframe.reset_index()

    toplot = []
    for i in range(len(country)):
        toplot.append(profile_to_plot.groupby('UserCountry').get_group(country[i]).reset_index(drop=True).mean().to_frame().transpose()[ambiguity_categories])
        #rename variables for more clarity on graph
        toplot[i] = toplot[i].rename(columns={'Ambiguity_SentientUtilitarianism': 'Sentient Utilitarianism','Ambiguity_ClassicalUtilitarianism': 'Classical Utilitarianism','Ambiguity_HedonisticUtilitarianism': '                 Hedonistic Utilitarianism','Ambiguity_AgentCenteredDeontology': 'Agent-Centered Deontology','Ambiguity_PatientCenteredDeontology': 'Patient-Centered Deontology','Ambiguity_ContractarianDeontology': 'Contractarian Deontology'})

    # number of variable
    categories=list(toplot[0])[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot[0].loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country[0])

    # values to plot
    values = toplot[1].loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country[1])

    #add legend if several users or comparison with average
    plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    #title
    plt.title("Ambiguity - Comparison between " + country[0] + " and " + country[1],fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()

def radar_chart_ambiguity_user_in_country(dataframe, country, rank):
    #radar chart for ambiguity comparison between one user and their country
    profile_to_plot = dataframe.reset_index()

    toplot = profile_to_plot.groupby('UserCountry').get_group(country).reset_index(drop=True)[ambiguity_categories]

    #rename variables for more clarity on graph

    toplot = toplot.rename(columns={'Ambiguity_SentientUtilitarianism': 'Sentient Utilitarianism','Ambiguity_ClassicalUtilitarianism': 'Classical Utilitarianism','Ambiguity_HedonisticUtilitarianism': '                 Hedonistic Utilitarianism','Ambiguity_AgentCenteredDeontology': 'Agent-Centered Deontology','Ambiguity_PatientCenteredDeontology': 'Patient-Centered Deontology','Ambiguity_ContractarianDeontology': 'Contractarian Deontology'})

    country_mean = toplot.mean().to_frame().transpose()
    datastd = profile_to_plot.groupby('UserCountry').get_group(country).reset_index(drop=True)[ambiguity_categories].std().to_frame().transpose()
    datastd['UserID'] = 0
    datastd = datastd[ambiguity_categories]
    datastd = datastd.rename(columns={'Ambiguity_SentientUtilitarianism': 'Sentient Utilitarianism','Ambiguity_ClassicalUtilitarianism': 'Classical Utilitarianism','Ambiguity_HedonisticUtilitarianism': '                 Hedonistic Utilitarianism','Ambiguity_AgentCenteredDeontology': 'Agent-Centered Deontology','Ambiguity_PatientCenteredDeontology': 'Patient-Centered Deontology','Ambiguity_ContractarianDeontology': 'Contractarian Deontology'})
    dataup = country_mean + datastd
    datainf = country_mean - datastd
    datainf[datainf < 0] = 0

    # number of variable
    categories=list(toplot)[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot.loc[int(rank)].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label="individual")

    # values to plot
    values = country_mean.loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country)

    #std to plot
    valuesup = dataup.loc[0].drop('UserID').values.flatten().tolist()
    valuesup += valuesup[:1]

    valueinf = datainf.loc[0].drop('UserID').values.flatten().tolist()
    valueinf += valueinf[:1]
    plt.fill_between(angles, valueinf, valuesup, alpha=0.5)

    #add legend if several users or comparison with average
    plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    #title
    plt.title("Ambiguity - User " + toplot['UserID'].loc[int(rank)] + " from " + country,fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()



        ###############
        ##  PROFILE  ##
        ###############


def radar_chart_profile(dataframe, rank_or_country='none', compare_to_average=False):
    #radar chart for ambiguity
    profile_to_plot = dataframe.reset_index()

    if rank_or_country == 'none':
        #this is for plotting averages
        global_average = profile_to_plot.mean().to_frame()
        global_average = global_average.transpose()

        #here is the dataframe for ambiguity average
        global_ambiguity = global_average[profile_categories]
        toplot = global_ambiguity
        rank = 0

        #useful dataframe for std
        stdbool = True
        datastd = dataframe.reset_index()

    else:
        try:
            #this is for plotting one user's profile
            #choose which one you want to see - rank between 0 and 51210
            rank = int(rank_or_country)
            #here is the dataframe for ambiguity average
            individual_ambiguity = profile_to_plot[profile_categories]
            toplot = individual_ambiguity

            #useful dataframe for std
            stdbool = False

        except ValueError:
            #this is for plotting one country's profile
            country_info = profile_to_plot.groupby('UserCountry').get_group(rank_or_country).reset_index(drop=True)
            toplot = country_info.mean().to_frame().transpose()[profile_categories]
            rank = 0

            #useful dataframe for std
            stdbool = True
            datastd = dataframe.reset_index()

    #rename variables for more clarity on graph

    toplot = toplot.rename(columns={'HowMuch_SentientUtilitarianism': 'Sentient Utilitarianism','HowMuch_ClassicalUtilitarianism': 'Classical Utilitarianism','HowMuch_HedonisticUtilitarianism': 'Hedonistic Utilitarianism','HowMuch_AgentCenteredDeontology': 'Agent-Centered Deontology','HowMuch_PatientCenteredDeontology': 'Patient-Centered Deontology','HowMuch_ContractarianDeontology': 'Contractarian Deontology'})


    # number of variable
    categories=list(toplot)[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot.loc[rank].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label="User")

    # optional fill
    #ax.fill(angles, values, 'b', alpha=0.1)

    if compare_to_average:
        #we can compare their profile to the average if needed
        global_average = profile_to_plot.mean().to_frame()
        global_average = global_average.transpose()

        #here is the dataframe for ambiguity average
        global_ambiguity = global_average[ambiguity_categories]

        values=global_ambiguity.loc[0].drop('UserID').values.flatten().tolist()
        values += values[:1]
        ax.plot(angles, values, linewidth=1, linestyle='solid', label="Average")

        #add legend if several users or comparison with average
        plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))
    elif stdbool :
        #this is for plotting std
        datastd = datastd.std().to_frame().transpose()
        datastd['UserID'] = 0
        datastd = datastd[profile_categories]
        datastd = datastd.rename(columns={'HowMuch_SentientUtilitarianism': 'Sentient Utilitarianism','HowMuch_ClassicalUtilitarianism': 'Classical Utilitarianism','HowMuch_HedonisticUtilitarianism': 'Hedonistic Utilitarianism','HowMuch_AgentCenteredDeontology': 'Agent-Centered Deontology','HowMuch_PatientCenteredDeontology': 'Patient-Centered Deontology','HowMuch_ContractarianDeontology': 'Contractarian Deontology'})
        dataup = toplot + datastd
        datainf = toplot - datastd
        datainf[datainf < 0] = 0

        valuesup = dataup.loc[0].drop('UserID').values.flatten().tolist()
        valuesup += valuesup[:1]

        valueinf = datainf.loc[0].drop('UserID').values.flatten().tolist()
        valueinf += valueinf[:1]
        plt.fill_between(angles, valueinf, valuesup, alpha=0.5)

    if rank_or_country == 'none':
        #title
        plt.title("Average profile",fontsize=12, y=1.0)
    else:
        try:
            individual = int(rank_or_country)
            #title
            plt.title("Profile - User " + profile_to_plot['UserID'].loc[individual] + " from " + profile_to_plot['UserCountry'].loc[individual],fontsize=12, y=1.0)
        except ValueError:
            #title
            plt.title("Profile - " + rank_or_country,fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()

def radar_chart_profile_2countries(dataframe, country):
    #radar chart for profile between 2 countries
    profile_to_plot = dataframe.reset_index()

    toplot = []
    for i in range(len(country)):
        toplot.append(profile_to_plot.groupby('UserCountry').get_group(country[i]).reset_index(drop=True).mean().to_frame().transpose()[profile_categories])
        #rename variables for more clarity on graph
        toplot[i] = toplot[i].rename(columns={'HowMuch_SentientUtilitarianism': 'Sentient Utilitarianism','HowMuch_ClassicalUtilitarianism': 'Classical Utilitarianism','HowMuch_HedonisticUtilitarianism': 'Hedonistic Utilitarianism','HowMuch_AgentCenteredDeontology': 'Agent-Centered Deontology','HowMuch_PatientCenteredDeontology': 'Patient-Centered Deontology','HowMuch_ContractarianDeontology': 'Contractarian Deontology'})

    # number of variable
    categories=list(toplot[0])[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot[0].loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country[0])

    # values to plot
    values = toplot[1].loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country[1])

    #add legend if several users or comparison with average
    plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    #title
    plt.title("Profile - Comparison between " + country[0] + " and " + country[1],fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()


def radar_chart_profile_user_in_country(dataframe, country, rank):
    #radar chart for profile comparison between one user and their country
    profile_to_plot = dataframe.reset_index()

    toplot = profile_to_plot.groupby('UserCountry').get_group(country).reset_index(drop=True)[profile_categories]

    #rename variables for more clarity on graph

    toplot = toplot.rename(columns={'HowMuch_SentientUtilitarianism': 'Sentient Utilitarianism','HowMuch_ClassicalUtilitarianism': 'Classical Utilitarianism','HowMuch_HedonisticUtilitarianism': 'Hedonistic Utilitarianism','HowMuch_AgentCenteredDeontology': 'Agent-Centered Deontology','HowMuch_PatientCenteredDeontology': 'Patient-Centered Deontology','HowMuch_ContractarianDeontology': 'Contractarian Deontology'})

    country_mean = toplot.mean().to_frame().transpose()
    datastd = profile_to_plot.groupby('UserCountry').get_group(country).reset_index(drop=True)[ambiguity_categories].std().to_frame().transpose()
    datastd['UserID'] = 0
    datastd = datastd[profile_categories]
    datastd = datastd.rename(columns={'HowMuch_SentientUtilitarianism': 'Sentient Utilitarianism','HowMuch_ClassicalUtilitarianism': 'Classical Utilitarianism','HowMuch_HedonisticUtilitarianism': 'Hedonistic Utilitarianism','HowMuch_AgentCenteredDeontology': 'Agent-Centered Deontology','HowMuch_PatientCenteredDeontology': 'Patient-Centered Deontology','HowMuch_ContractarianDeontology': 'Contractarian Deontology'})
    dataup = country_mean + datastd
    datainf = country_mean - datastd
    datainf[datainf < 0] = 0


    # number of variable
    categories=list(toplot)[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot.loc[int(rank)].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label="individual")

    # values to plot
    values = country_mean.loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country)

    #std to plot
    valuesup = dataup.loc[0].drop('UserID').values.flatten().tolist()
    valuesup += valuesup[:1]

    valueinf = datainf.loc[0].drop('UserID').values.flatten().tolist()
    valueinf += valueinf[:1]
    plt.fill_between(angles, valueinf, valuesup, alpha=0.5)

    #add legend if several users or comparison with average
    plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    #title
    plt.title("Profile - User " + toplot['UserID'].loc[int(rank)] + " from " + country,fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()


        ###############
        ##   VOTES   ##
        ###############



def radar_chart_election(dataframe, rank_or_country='none', compare_to_average=False):
    #radar chart for ambiguity
    profile_to_plot = dataframe.reset_index()

    if rank_or_country == 'none':
        #this is for plotting averages
        global_average = profile_to_plot.mean().to_frame()
        global_average = global_average.transpose()

        #here is the dataframe for ambiguity average
        global_ambiguity = global_average[votes]
        toplot = global_ambiguity
        rank = 0

        #useful dataframe for std
        stdbool = True
        datastd = dataframe.reset_index()

    else:
        try:
            #this is for plotting one user's profile
            #choose which one you want to see - rank between 0 and 51210
            rank = int(rank_or_country)
            #here is the dataframe for ambiguity average
            individual_ambiguity = profile_to_plot[votes]
            toplot = individual_ambiguity

            #useful dataframe for std
            stdbool = False

        except ValueError:
            #this is for plotting one country's profile
            country_info = profile_to_plot.groupby('UserCountry').get_group(rank_or_country).reset_index(drop=True)
            toplot = country_info.mean().to_frame().transpose()[votes]
            rank = 0

            #useful dataframe for std
            stdbool = True
            datastd = dataframe.reset_index()

    # number of variable
    categories=list(toplot)[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot.loc[rank].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label="User")

    # optional fill
    #ax.fill(angles, values, 'b', alpha=0.1)

    if compare_to_average:
        #we can compare their profile to the average if needed
        global_average = profile_to_plot.mean().to_frame()
        global_average = global_average.transpose()

        #here is the dataframe for ambiguity average
        global_ambiguity = global_average[votes]

        values=global_ambiguity.loc[0].drop('UserID').values.flatten().tolist()
        values += values[:1]
        ax.plot(angles, values, linewidth=1, linestyle='solid', label="Average")

        #add legend if several users or comparison with average
        plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    elif stdbool :
        #this is for plotting std
        datastd = datastd.std().to_frame().transpose()
        datastd['UserID'] = 0
        datastd = datastd[votes]
        dataup = toplot + datastd
        datainf = toplot - datastd
        datainf[datainf < 0] = 0

        valuesup = dataup.loc[0].drop('UserID').values.flatten().tolist()
        valuesup += valuesup[:1]

        valueinf = datainf.loc[0].drop('UserID').values.flatten().tolist()
        valueinf += valueinf[:1]
        plt.fill_between(angles, valueinf, valuesup, alpha=0.5)

    if rank_or_country == 'none':
        #title
        plt.title("Average votes",fontsize=12, y=1.0)
    else:
        try:
            individual = int(rank_or_country)
            #title
            plt.title("Votes - User " + profile_to_plot['UserID'].loc[individual] + " from " + profile_to_plot['UserCountry'].loc[individual],fontsize=12, y=1.0)
        except ValueError:
            #title
            plt.title("Votes - " + rank_or_country,fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()


def radar_chart_election_user_in_country(dataframe, country, rank):
    #radar chart for profile comparison between one user and their country
    profile_to_plot = dataframe.reset_index()

    toplot = profile_to_plot.groupby('UserCountry').get_group(country).reset_index(drop=True)[votes]

    country_mean = toplot.mean().to_frame().transpose()
    # number of variable
    categories=list(toplot)[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot.loc[int(rank)].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label="individual")

    # values to plot
    values = country_mean.loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country)

    #add legend if several users or comparison with average
    plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    #title
    plt.title("Votes - User " + toplot['UserID'].loc[int(rank)] + " from " + country,fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()

def radar_chart_election_2countries(dataframe, country):
    #radar chart for profile between 2 countries
    profile_to_plot = dataframe.reset_index()

    toplot = []
    for i in range(len(country)):
        toplot.append(profile_to_plot.groupby('UserCountry').get_group(country[i]).reset_index(drop=True).mean().to_frame().transpose()[votes])
        #rename variables for more clarity on graph
        #toplot[i] = toplot[i].rename(columns={'HowMuch_SentientUtilitarianism': 'Sentient Utilitarianism','HowMuch_ClassicalUtilitarianism': 'Classical Utilitarianism','HowMuch_HedonisticUtilitarianism': 'Hedonistic Utilitarianism','HowMuch_AgentCenteredDeontology': 'Agent-Centered Deontology','HowMuch_PatientCenteredDeontology': 'Patient-Centered Deontology','HowMuch_ContractarianDeontology': 'Contractarian Deontology'})

    # number of variable
    categories=list(toplot[0])[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot[0].loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country[0])

    # values to plot
    values = toplot[1].loc[0].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=country[1])

    #add legend if several users or comparison with average
    plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    #title
    plt.title("Votes - Comparison between " + country[0] + " and " + country[1],fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()


        ###############
        ## CREDENCES ##
        ###############


def radar_chart_credences(dataframe, rank_or_country='none', compare_to_average=False):
    #radar chart for ambiguity
    profile_to_plot = dataframe.reset_index()

    if rank_or_country == 'none':
        #this is for plotting averages
        global_average = profile_to_plot.mean().to_frame()
        global_average = global_average.transpose()

        #here is the dataframe for ambiguity average
        global_ambiguity = global_average[credences]
        toplot = global_ambiguity
        rank = 0

        #useful dataframe for std
        stdbool = True
        datastd = dataframe.reset_index()
    else:
        try:
            #this is for plotting one user's profile
            #choose which one you want to see - rank between 0 and 51210
            rank = int(rank_or_country)
            #here is the dataframe for ambiguity average
            individual_ambiguity = profile_to_plot[credences]
            toplot = individual_ambiguity

            #useful dataframe for std
            stdbool = False

        except ValueError:
            #this is for plotting one country's profile
            country_info = profile_to_plot.groupby('UserCountry').get_group(rank_or_country).reset_index(drop=True)
            toplot = country_info.mean().to_frame().transpose()[credences]
            rank = 0

            #useful dataframe for std
            stdbool = True
            datastd = dataframe.reset_index()

    # number of variable
    categories=list(toplot)[1:]
    N = len(categories)

    # angle of each axis in the plot (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # first axis to be on top:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([0.25,0.5,0.75], ["0.25","0.5","0.75"], color="grey", size=8)
    plt.ylim(0,1)

    # values to plot
    values = toplot.loc[rank].drop('UserID').values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label="User")

    # optional fill
    #ax.fill(angles, values, 'b', alpha=0.1)

    if compare_to_average:
        #we can compare their profile to the average if needed
        global_average = profile_to_plot.mean().to_frame()
        global_average = global_average.transpose()

        #here is the dataframe for ambiguity average
        global_ambiguity = global_average[credences]

        values=global_ambiguity.loc[0].drop('UserID').values.flatten().tolist()
        values += values[:1]
        ax.plot(angles, values, linewidth=1, linestyle='solid', label="Average")

        #add legend if several users or comparison with average
        plt.legend(loc='lower right', bbox_to_anchor=(0.1, 0.1))

    elif stdbool :
        #this is for plotting std
        datastd = datastd.std().to_frame().transpose()
        datastd['UserID'] = 0
        datastd = datastd[credences]
        dataup = toplot + datastd
        datainf = toplot - datastd
        datainf[datainf < 0] = 0

        valuesup = dataup.loc[0].drop('UserID').values.flatten().tolist()
        valuesup += valuesup[:1]

        valueinf = datainf.loc[0].drop('UserID').values.flatten().tolist()
        valueinf += valueinf[:1]
        plt.fill_between(angles, valueinf, valuesup, alpha=0.5)

    if rank_or_country == 'none':
        #title
        plt.title("Average credences",fontsize=12, y=1.0)
    else:
        try:
            individual = int(rank_or_country)
            #title
            plt.title("credences - User " + profile_to_plot['UserID'].loc[individual] + " from " + profile_to_plot['UserCountry'].loc[individual],fontsize=12, y=1.0)
        except ValueError:
            #title
            plt.title("credences - " + rank_or_country,fontsize=12, y=1.0)
    plt.tight_layout()
    return ax.get_figure()
