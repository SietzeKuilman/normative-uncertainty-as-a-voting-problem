import numpy as np
import pandas as pd
from .classifier import *
from .principles import *
from .election import *
from tqdm import tqdm, tqdm_notebook

#additional tests

#def uncovered_answers_utility(x):
    ##outputs a new column with answers that aren't explained with any of our principles (neither pro or against)
#    y = x
#    if y.Ambiguity_HedonisticUtilitarianism == -1 and y.Ambiguity_ClassicalUtilitarianism == -1 and y.Ambiguity_AgentCenteredDeontology == -1 and y.Ambiguity_SentientUtilitarianism == -1 and y.Ambiguity_PatientCenteredDeontology == -1 and y.Ambiguity_ContractarianDeontology == -1:
#        return 1
#    return 0


def ethical_answers(x, converted=False):
    # used to see the moral score for each decision made
    # 1 if pro, 0 if against, -1 if can't tell
    # the opposite choice would obviously have opposite scores
    
    # check if converted or not - only to gain some time
    if converted:
        y = x.copy()
    else:
        y = conversion(x.copy())
    
    tqdm_notebook().pandas()
    # apply our functions
    y['Sentient_U'] = y.progress_apply(SentientUtilitarianism_utility, axis=1)
    y['Classical_U'] = y.progress_apply(ClassicalUtilitarianism_utility, axis=1)
    y['Hedonistic_U'] = y.progress_apply(HedonisticUtilitarianism_utility, axis=1)
    y['AgentCentered_D'] = y.progress_apply(AgentCenteredDeontology_utility, axis=1)
    y['PatientCentered_D'] = y.progress_apply(PatientCenteredDeontology_utility, axis=1)
    y['Contractarian_D'] = y.progress_apply(ContractarianDeontology_utility, axis=1)
    
    #y['Uncovered_answers'] = y.progress_apply(uncovered_answers_utility, axis=1)
    
    result = y.set_index('ResponseID', drop=True)
    result = result[['Sentient_U', 'Classical_U', 'Hedonistic_U', 'AgentCentered_D', 'PatientCentered_D', 'Contractarian_D']]
    return result


def ethical_multiplicity(x, converted=False):
    # used for clearer information
    # use previous function 'ethical_answers' to output some more readable information
    # ouputs how many ethical theories the decision respected and how many it did not
    # also outputs how many utilitarian or deontological theories he respected among the 3
    
    if converted:
        y = x.copy()
    else:
        y = ethical_answers(x.copy())

    pros = y.copy()
    pros = pros.replace(to_replace = -1, value = 0)
    tqdm_notebook().pandas()
    pros['HowManyTheoriesPro'] = pros.progress_apply(np.sum, axis=1)
    pros['HowManyPro_U'] = pros.Sentient_U + pros.Classical_U + pros.Hedonistic_U
    pros['HowManyPro_D'] = pros.AgentCentered_D + pros.PatientCentered_D + pros.Contractarian_D
    pros = pros[['HowManyTheoriesPro', 'HowManyPro_U', 'HowManyPro_D']]
    
    cons = y.copy()
    cons = cons.replace(to_replace = 1, value = -1)
    cons = cons.replace(to_replace = 0, value = 1)
    cons = cons.replace(to_replace = -1, value = 0)
    tqdm_notebook().pandas()
    cons['HowManyTheoriesCons'] = cons.progress_apply(np.sum, axis=1)
    cons['HowManyTheoriesCons'] = cons.HowManyTheoriesCons
    cons['HowManyAgainst_U'] = (cons.Sentient_U + cons.Classical_U + cons.Hedonistic_U)
    cons['HowManyAgainst_D'] = (cons.AgentCentered_D + cons.PatientCentered_D + cons.Contractarian_D)
    cons = cons[['HowManyTheoriesCons', 'HowManyAgainst_U', 'HowManyAgainst_D']]
    
    result = pros.merge(cons, left_index=True, right_index=True)
    return result


        ###############
        ##   BORDA   ##
        ###############


def borda_against(x):
    y = x
    if y.Borda_wins == 0:
        return y.HowManyTheoriesPro
    else:
        return y.HowManyTheoriesCons

def borda_pro(x):
    y = x
    if y.Borda_wins == 0:
        return y.HowManyTheoriesCons
    else:
        return y.HowManyTheoriesPro

def borda_implication(x, converted=False, level='individual'):
    # ouputs how many ethical theories are violated using borda
    # if society_level=True then society_borda_count is used (average credences are used)
    
    if converted:
        y = x.copy()
    else:
        y = conversion(x.copy())
    
    if level=='society':
        print('Returning society level - computed with average credences')
        borda_df = society_borda_count(y.copy(), converted=True)
    elif level=='principles':
        print('Returning principles level - computed without credences')
        borda_df = borda_count(y.copy(), converted=True, without_credences=True)
    else:
        print('Returning default choice - individual level')
        borda_df = borda_count(y.copy(), converted=True)
    
    voting_df = borda_df.set_index('ResponseID', drop=True)
    
    multiple = ethical_multiplicity(ethical_answers(y.copy(), converted=True), converted=True)
    result = pd.concat([voting_df, multiple], axis=1, join='inner')
    
    result['TheoriesAgainst'] = result.apply(borda_against, axis=1)
    result['TheoriesPro'] = result.apply(borda_pro, axis=1)
    result = result[['TheoriesAgainst','TheoriesPro']]
    
    return result


        ################
        ##  MAJORITY  ##
        ################


def majority_against(x):
    y = x
    if y.Majority_wins == 0:
        return y.HowManyTheoriesPro
    else:
        return y.HowManyTheoriesCons

def majority_pro(x):
    y = x
    if y.Majority_wins == 0:
        return y.HowManyTheoriesCons
    else:
        return y.HowManyTheoriesPro

def majority_implication(x, converted=False, level='individual'):
    # ouputs how many ethical theories are violated using majority voting scheme
    # if society_level=True then society_majority_count is used (average credences are used)
    
    if converted:
        y = x.copy()
    else:
        y = conversion(x.copy())
    
    if level=='society':
        print('Returning society level - computed with average credences')
        majority_df = society_majority_count(y.copy(), converted=True)
    elif level=='principles':
        print('Returning principles level - computed without credences')
        majority_df = majority_count(y.copy(), converted=True, without_credences=True)
    else:
        print('Returning default choice - individual level')
        majority_df = majority_count(y.copy(), converted=True)
    
    voting_df = majority_df.set_index('ResponseID', drop=True)
    
    multiple = ethical_multiplicity(ethical_answers(y.copy(), converted=True), converted=True)
    result = pd.concat([voting_df, multiple], axis=1, join='inner')
    
    result['TheoriesAgainst'] = result.apply(majority_against, axis=1)
    result['TheoriesPro'] = result.apply(majority_pro, axis=1)
    result = result[['TheoriesAgainst','TheoriesPro']]
    return result

        #############
        ##   MFT   ##
        #############


def mft_against(x):
    y = x
    if y.MFT_score == 0:
        return y.HowManyTheoriesPro
    else:
        return y.HowManyTheoriesCons

def mft_pro(x):
    y = x
    if y.MFT_score == 0:
        return y.HowManyTheoriesCons
    else:
        return y.HowManyTheoriesPro

def my_favourite_theory_implication(x, converted=False, level='individual'):
    # ouputs how many ethical theories are violated using majority voting scheme
    # if society_level=True then society_majority_count is used (average credences are used)
    
    if converted:
        y = x.copy()
    else:
        y = conversion(x.copy())
    
    if level=='society':
        print('Returning society level - computed with average credences')
        mft_df = society_my_favourite_theory_count(y.copy(), converted=True, evaluation=True)
    else:
        print('Returning default choice - individual level')
        mft_df = my_favourite_theory_count(y.copy(), converted=True, evaluation=True)
    
    voting_df = mft_df.set_index('ResponseID', drop=True)
    
    multiple = ethical_multiplicity(ethical_answers(y.copy(), converted=True), converted=True)
    result = pd.concat([voting_df, multiple], axis=1, join='inner')
    
    result['TheoriesAgainst'] = result.apply(mft_against, axis=1)
    result['TheoriesPro'] = result.apply(mft_pro, axis=1)
    result = result[['TheoriesAgainst','TheoriesPro']]
    return result
